import com.gitlab.survivalRaiz.landProtection.util.ArrayTable;

/**
 * Simple class to test the implementations.
 * This should use a tests API, but I am too lazy to learn how to use one so this will have to work.
 */
public class ArrayTableTester {
    public static void main(String[] args) {
        testImplementation();
    }

    private static void testImplementation() {
        final ArrayTable<Integer, Integer> at =
                new ArrayTable<>(new Integer[0], new Integer[0][], Integer.class, Integer.class);
        System.out.println(at.toString());

        System.out.println("!!! INSERT !!!");
        at.insert(2, 2);
        System.out.println(">> "+at.toString());
        at.insert(3, 2);
        System.out.println(">> "+at.toString());
        at.insert(2, 3);
        System.out.println(">> "+at.toString());
        at.insert(2, 1);
        System.out.println(">> "+at.toString());
        at.insert(1, 1);
        System.out.println(">> "+at.toString());
        at.insert(1, 1);
        System.out.println(">> "+at.toString());

        System.out.println("!!! REMOVE !!!");
        at.remove(2, 2);
        System.out.println(">> "+at.toString());
        at.remove(2, 3);
        System.out.println(">> "+at.toString());
        at.remove(3, 2);
        System.out.println(">> "+at.toString());
        at.remove(1, 1);
        System.out.println(">> "+at.toString());
        at.remove(2, 1);
        System.out.println(">> "+at.toString());
        at.remove(1, 1);
        System.out.println(">> "+at.toString());
    }
}
