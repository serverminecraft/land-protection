import com.gitlab.survivalRaiz.landProtection.util.ArrayTable;
import util.TriConsumer;

import java.util.*;

public class ArrTableBenchmark {
    public static void main(String[] args) {
        final int repetitions = 10;
        final int dataLen = 100000;
        final long[][] results = new long[repetitions][dataLen];

        System.out.println("||== TESTING ARRAY TABLE ==||");
        benchGeneric(repetitions, dataLen, results);

        System.out.println("\n\n||== TESTING MAP ==||");
        benchMap(repetitions, dataLen, results);
    }

    private static void benchMap(int repetitions, int dataLen, long[][] results) {
        final List<Map<Integer, Integer>> tab = new ArrayList<>();
        for (int i = 0; i < repetitions; i++)
            tab.add(new HashMap<>());

        final int[][][] data = new int[repetitions][dataLen][2];
        final Random rng = new Random();
        final int max = 10000000;
        final int min = -10000000;

        System.out.println("Initializing data set...");
        for (int i = 0; i < repetitions; i++) {
            for (int i1 = 0; i1 < dataLen; i1++) {
                data[i][i1] =
                        new int[]{rng.nextInt((max - min) + 1) + min, rng.nextInt((max - min) + 1) + min};
            }
        }

        unitMap("Insertion", repetitions, dataLen, data, results, tab, Map::put);

        System.out.println("shuffling data set...");
        for (int i = 0; i < repetitions; shuffleArray(data[i++])) ;

        unitMap("Search", repetitions, dataLen, data, results, tab,
                (map, a, b) -> {
                    if (map.get(a) != null)
                        map.get(a).equals(b);
                });

        System.out.println("shuffling data set...");
        for (int i = 0; i < repetitions; shuffleArray(data[i++])) ;

        unitMap("Deletion", repetitions, dataLen, data, results, tab,
                (map, a, b) -> {
                    if (map.get(a) == null) return;
                    if (map.get(a).equals(b))
                        map.remove(a);
                });
    }

    private static void benchGeneric(int repetitions, int dataLen, long[][] results) {
        final List<ArrayTable<Integer, Integer>> tab = new ArrayList<>();
        for (int i = 0; i < repetitions; i++)
            tab.add(new ArrayTable<>(new Integer[0], new Integer[0][], Integer.class, Integer.class));

        final int[][][] data = new int[repetitions][dataLen][2];
        final Random rng = new Random();
        final int max = 10000000;
        final int min = -10000000;

        System.out.println("Initializing data set...");
        for (int i = 0; i < repetitions; i++) {
            for (int i1 = 0; i1 < dataLen; i1++) {
                data[i][i1] =
                        new int[]{rng.nextInt((max - min) + 1) + min, rng.nextInt((max - min) + 1) + min};
            }
        }

        unitArrTabGen("Insertion", repetitions, dataLen, data, results, tab, ArrayTable::insert);

        System.out.println("shuffling data set...");
        for (int i = 0; i < repetitions; shuffleArray(data[i++])) ;

        unitArrTabGen("Search", repetitions, dataLen, data, results, tab, ArrayTable::contains);

        System.out.println("shuffling data set...");
        for (int i = 0; i < repetitions; shuffleArray(data[i++])) ;

        unitArrTabGen("Deletion", repetitions, dataLen, data, results, tab, ArrayTable::remove);
    }

    private static void unitMap(String name, int repetitions, int dataLen, int[][][] data, long[][] results,
                                List<Map<Integer, Integer>> tabList,
                                TriConsumer<Map<Integer, Integer>, Integer, Integer> action) {
        System.out.println("Benching " + name + "...");

        for (int i = 0; i < repetitions; i++) {
            final Map<Integer, Integer> tab = tabList.get(i);
            for (int i1 = 0; i1 < dataLen; i1++) {
                final long start = System.nanoTime();
                action.accept(tab, data[i][i1][0], data[i][i1][1]);
                final long end = System.nanoTime();
                results[i][i1] = end - start;
            }
        }

        System.out.println("==================");
        System.out.println(name + " Results:");
        for (int i = 0; i < repetitions; i++) {
            final double avg = findAverageUsingStream(results[i]);
            System.out.println("    " + avg + "ns");
        }
        System.out.println("==================");
    }

    private static void unitArrTabGen(String name, int repetitions, int dataLen, int[][][] data, long[][] results,
                                      List<ArrayTable<Integer, Integer>> tabList,
                                      TriConsumer<ArrayTable<Integer, Integer>, Integer, Integer> action) {
        System.out.println("Benching " + name + "...");

        for (int i = 0; i < repetitions; i++) {
            final ArrayTable<Integer, Integer> tab = tabList.get(i);
            for (int i1 = 0; i1 < dataLen; i1++) {
                final long start = System.nanoTime();
                action.accept(tab, data[i][i1][0], data[i][i1][1]);
                final long end = System.nanoTime();
                results[i][i1] = end - start;
            }
        }

        System.out.println("==================");
        System.out.println(name + " Results:");
        for (int i = 0; i < repetitions; i++) {
            final double avg = findAverageUsingStream(results[i]);
            System.out.println("    " + avg + "ns");
        }
        System.out.println("==================");
    }

    private static void shuffleArray(int[][] ar) {
        final Random rng = new Random();
        for (int i = ar.length - 1; i > 0; i--) {
            final int index = rng.nextInt(i + 1);

            // Simple swap
            final int[] a = ar[index];
            ar[index] = ar[i];
            ar[i] = a;
        }
    }

    public static double findAverageUsingStream(long[] array) {
        return Arrays.stream(array).average().orElse(Double.NaN);
    }
}
