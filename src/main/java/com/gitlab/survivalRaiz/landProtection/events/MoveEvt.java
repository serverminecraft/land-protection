package com.gitlab.survivalRaiz.landProtection.events;

import com.gitlab.survivalRaiz.landProtection.terrain.LandManager;
import com.gitlab.survivalRaiz.landProtection.terrain.Terrain;
import org.bukkit.Location;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.plugin.java.JavaPlugin;

public class MoveEvt implements Listener {
    private final LandManager landManager;

    public MoveEvt(JavaPlugin plugin, LandManager landManager) {
        this.landManager = landManager;
        plugin.getServer().getPluginManager().registerEvents(this, plugin);
    }

    @EventHandler
    public void onMove(PlayerMoveEvent event){
        try {
            if ((event.getFrom().getChunk().getX()) == (event.getTo().getChunk().getX()) &&
                    (event.getFrom().getChunk().getZ()) == (event.getTo().getChunk().getZ()))
                return;
        } catch (NullPointerException npe) {
            event.setCancelled(true);
        }

        final Location l = event.getTo();
        assert l != null;
        final int x = l.getChunk().getX();
        final int y = l.getChunk().getZ();

        if (landManager.isTerrain(x, y))
            for (Terrain t : landManager.fromUser(event.getPlayer().getUniqueId()))
                if (t.contains(l))
                    event.setCancelled(!t.getOwner().equals(event.getPlayer().getUniqueId()));
    }
}
