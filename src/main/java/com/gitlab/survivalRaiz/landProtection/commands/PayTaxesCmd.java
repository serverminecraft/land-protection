package com.gitlab.survivalRaiz.landProtection.commands;

import api.skwead.commands.ConfigCommand;
import api.skwead.exceptions.exceptions.CommandException;
import com.gitlab.survivalRaiz.core.excepions.SRCommandException;
import com.gitlab.survivalRaiz.core.messages.Message;
import com.gitlab.survivalRaiz.landProtection.LandProtection;
import com.gitlab.survivalRaiz.landProtection.terrain.Terrain;
import com.gitlab.survivalRaiz.permissions.management.Perms;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;

public class PayTaxesCmd extends ConfigCommand {
    private final LandProtection plugin;

    public PayTaxesCmd(LandProtection plugin) {
        super("imposto", "paga o imposto do terreno", "/imposto <terreno> [meses]", new ArrayList<>(), "imposto");
        this.plugin = plugin;
    }

    @Override
    public int validate(CommandSender commandSender, String s, String[] strings) throws CommandException {
        if (!(commandSender instanceof Player))
            throw new SRCommandException(commandSender, Message.PLAYER_ONLY_COMMAND, plugin.getCore());

        final Player p = (Player) commandSender;
        final double pay;

        if (strings.length < 1)
            throw new SRCommandException(p, Message.SYNTAX, getUsage(), plugin.getCore());

        try {
            final int id = Integer.parseInt(strings[0]);
            final Terrain t = plugin.getLandManager().fromDb(id);

            if (t == null)
                throw new SRCommandException(p, Message.TERRAIN_DOES_NOT_EXIST, plugin.getCore());

            if (!t.getOwner().equals(p.getUniqueId()) && !Perms.PAY_OTHERS_TAXES.allows(p))
                throw new SRCommandException(p, Message.TERRAIN_IS_NOT_YOURS, plugin.getCore());

            if (strings.length == 1)
                pay = plugin.getCostsConfig().calculate(t);
            else
                pay = plugin.getCostsConfig().calculate(t) * Integer.parseInt(strings[1]);

        } catch (NumberFormatException e) {
            throw new SRCommandException(p, Message.SYNTAX, getUsage(), plugin.getCore());
        }

        if (plugin.getEconomyPlugin().getEconomy().has(p.getUniqueId(), pay))
            return strings.length == 1 ? 0 : 1;
        else
            throw new SRCommandException(p, Message.PLAYER_BROKE, plugin.getCore());
    }

    @Override
    public void run(CommandSender commandSender, String s, String[] strings) throws CommandException {
        final int syntax = validate(commandSender, s, strings);
        final int id = Integer.parseInt(strings[0]);
        final double pay = syntax == 0 ?
                plugin.getCostsConfig().calculate(plugin.getLandManager().fromDb(id)) :
                plugin.getCostsConfig().calculate(plugin.getLandManager().fromDb(id)) * Integer.parseInt(strings[1]);

        plugin.getEconomyPlugin().getEconomy().withdrawPlayer(((OfflinePlayer) commandSender).getUniqueId(), pay);
    }
}
