package com.gitlab.survivalRaiz.landProtection.util;

import java.lang.reflect.Array;
import java.util.Arrays;

/**
 * A generalized data structure to map single keys to multiple values.
 * This is similar to a HashTable, but stores the data in arrays for the fastest possible search.
 * Does not support duplicated key value pairs.
 * Insertion is slower due to array expansion.
 * All search implements binary search, and the arrays are dimensioned dynamically and sorted at insertion and removal.
 *
 * @param <K> the Key type
 * @param <V> the Value type
 */
public class ArrayTable<K extends Comparable<K>, V extends Comparable<V>> {
    private K[] keys;
    private V[][] values;
    private final Class<K> keyClass;
    private final Class<V> valueClass;

    public ArrayTable(K[] keys, V[][] values, Class<K> keyClass, Class<V> valueClass) {
        this.keys = keys;
        this.values = values;
        this.keyClass = keyClass;
        this.valueClass = valueClass;
    }

    public boolean contains(K key, V value) {
        final V[] o = get(key);
        return o != null && Arrays.binarySearch(o, value) >= 0;
//        if (o == null) return false;
//        return Arrays.binarySearch(o, value) >= 0;
    }

    @SuppressWarnings("unchecked")
    public void insert(K key, V value) {
        if (contains(key, value)) return;

        if (get(key) == null) {
//            System.out.println("is null");
            K[] k = (K[]) Array.newInstance(this.keyClass, this.keys.length + 1);
            System.arraycopy(this.keys, 0, k, 0, keys.length);
            k[k.length - 1] = key;
            this.keys = k;
//            System.out.println(Arrays.toString(this.keys));

            V[] v = (V[]) Array.newInstance(this.valueClass, 1);
            v[0] = value;
            V[][] n = (V[][]) Array.newInstance(v.getClass(), this.values.length + 1);
            System.arraycopy(this.values, 0, n, 0, values.length);
            n[this.values.length] = v;
            this.values = n;
//            System.out.println(Arrays.deepToString(this.values));

            sort();
        } else {
            V[] vs = this.get(key);
//            System.out.println(Arrays.toString(vs));
            V[] v = (V[]) Array.newInstance(this.valueClass, vs.length + 1);
//            System.out.println(Arrays.toString(v));
            System.arraycopy(vs, 0, v, 0, vs.length);
            v[v.length - 1] = value;
            sort(v);
            set(key, v);
        }
    }

    public void remove(K key, V value) {
        V[] rem = get(key);
        if (rem == null) return;
        if (Arrays.binarySearch(rem, value) < 0) return;

        rem = removeInArray(rem, value, valueClass);

        if (rem.length == 0){
            this.keys = removeInArray(this.keys, key, keyClass);
            this.values = removeInMatrix(this.values, rem, valueClass);
        } else {
            set(key, rem);
        }
    }

    @SuppressWarnings("unchecked")
    private <E extends Comparable<E>> E[] removeInArray(E[] original, E elem, Class<E> type) {
        int i = Arrays.binarySearch(original, elem);
        if (i < 0) return original;

        while (i < original.length - 1)
            original[i] = original[++i];

        E[] res = (E[]) Array.newInstance(type, original.length - 1);
        System.arraycopy(original, 0, res, 0, res.length);

        return res;
    }

    @SuppressWarnings("unchecked")
    private <E extends Comparable<E>> E[][] removeInMatrix(E[][] original, E[] elem, Class<E> type) {
        for (int i = 0; i < original.length; i++)
            if (Arrays.equals(original[i], elem))
                while (i < original.length - 1)
                    original[i] = original[++i];

        E[][] res = (E[][]) Array.newInstance(type, original.length - 1, 0);
        System.arraycopy(original, 0, res, 0, original.length - 1);

        return res;
    }

    private <T extends Comparable<T>> void sort(T[] arr) {
        int n = arr.length;
        for (int i = 1; i < n; ++i) {
            T key = arr[i];
            int j = i - 1;

            while (j >= 0 && arr[j].compareTo(key) > 0) {
                arr[j + 1] = arr[j];
                j = j - 1;
            }
            arr[j + 1] = key;
        }
    }

    private void sort() {
        int n = keys.length;
        for (int j = 1; j < n; j++) {
            K key = keys[j];
            V[] value = values[j];
            int i = j - 1;
            while ((i > -1) && (keys[i].compareTo(key) > 0)) {
                keys[i + 1] = keys[i];
                values[i + 1] = values[i];
                i--;
            }
            keys[i + 1] = key;
            values[i + 1] = value;
        }
    }

    private void set(K key, V[] n) {
        final int i = Arrays.binarySearch(keys, key);
        if (i < 0) return;
        if (values.length == 0) return;
        values[i] = n;
    }

    public V[] get(K key) {
        final int i = Arrays.binarySearch(keys, key);
        if (i < 0) return null;
        if (values.length == 0) return null;
        return values[i];
    }

    @Override
    public String toString() {
        return "ArrayTable{" +
                "keys=" + Arrays.toString(keys) +
                ", values=" + Arrays.deepToString(values) +
                ", keyClass=" + keyClass +
                ", valueClass=" + valueClass +
                '}';
    }
}
